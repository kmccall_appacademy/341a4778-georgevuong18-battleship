class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(pos)
    #@board.grid[pos.first][pos.last] = :x
    #p @board[pos]
    p @board[pos] = :x
  end

  def count
    p @board.count
  end

  def game_over?
    p @board.won?
  end

  def play_turn
    pos = @player.get_play
    attack(pos)
  end
end
