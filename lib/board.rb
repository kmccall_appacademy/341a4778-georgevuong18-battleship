class Board
  attr_reader :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  # def count
  #   count = 0
  #   self.grid.flatten.each { |pos| count += 1 unless pos.nil? }
  #   count
  # end

  def empty?(pos = [])
    if pos != [] && self.grid[pos.first][pos.last] == nil
      true
    elsif self.grid.flatten.all? { |posi| posi == nil }
      true
    end
  end

  def full?
    true if self.grid.flatten.none? { |pos| pos == nil }
  end

  def place_random_ship
    raise "error" if full?
    grid_idx = (0...@grid.length).to_a + (0...@grid[0].length).to_a
    possible_pos = grid_idx.permutation(2).to_a.uniq
    rand_pos = possible_pos.sample
    @grid[rand_pos.first][rand_pos.last] = :s
  end

  def won?
    true if self.grid.flatten.all? { |pos| pos == nil }
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
    #alternate method
    # row, col = pos
    # @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end
end
